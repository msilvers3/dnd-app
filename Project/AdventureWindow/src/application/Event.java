package application;
/**
 * event class that calls the event model to generate an event.
 * @author TeamOmega
 *
 */
public class Event {
	private Player player;
	private Location location;
	
	
	public Event(Player player, Location location) {
		this.player = player;
		this.location = location;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	public Location getLocation() {
		return this.location;
	}
}
