package application;
import java.util.ArrayList;
/**
 * the item model pulls the item from the item class and it stores it in an array for other classes to pull from
 * @author TeamOmega
 *
 */
public class ItemModel {
	private ArrayList<Item> items;
	
	
	public ItemModel() {
		this.items = new ArrayList<>();
	}
	
	public void addItem(Item item) {
		this.items.add(item);
	}
	
	public Item getItem(int index) {
		return this.items.get(index);
	}
	
	public ArrayList<Item> getItemArrayList() {
		return this.items;
	}
	
	@Override
	public String toString() {
		String out = "";
		
		for(Item item:this.items) {
			out+=item.toString();
		}
		
		return out;
	}
}
