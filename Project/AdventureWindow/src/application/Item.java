package application;
/**
 * the item class is responsible for generating an item for the discovery event class to use
 * @author TeamOmega
 *
 */
public class Item {
	private String itemName;
	private String description;
	
	public Item(String name, String desc) {
		this.itemName = name;
		this.description = desc;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return String.format(
				"Name: %s\n"
				+ "Description: %s\n",
				this.itemName,
				this.description);
	}
	
	
}
