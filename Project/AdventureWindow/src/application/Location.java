package application;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * this class will make a list of towns and store them in an array that the player character can visit.
 * @author TeamOmega
 *
 */
public class Location {
	private ArrayList<String> townList;
	private String genericGeoSelected;
	private String[] genericGeographicPlaces = {"mountain","river", "valley"
												, "road", "path", "jungle"
												, "desert", "ravine", "lake"
												, "spring", "cliff"};
	private String currentTown;
	
	public Location() {
		this.townList = new ArrayList<>();
		generateTowns();
		generateTown();
		generateRandomGeographicLocation();
	}
	/**
	 * Method to get town names from text file
	 */
	private void generateTowns() {
		try { 
			BufferedReader reader = new BufferedReader(new FileReader("src/application/townNameList"));
			String line;
			while((line = reader.readLine()) != null) {
				this.townList.add(line);
			}
			reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		 
	}
	/**
	 * Method to go to the next town
	 */
	public void nextTown() {
		int currentIndex = this.townList.indexOf(this.currentTown);
		if (currentIndex == this.townList.size()-1) {
			currentIndex = 0;
			this.currentTown = this.townList.get(currentIndex);
		}
		else {
			this.currentTown = this.townList.get(currentIndex+1);			
		}
	}
	
	
	public void generateRandomGeographicLocation() {
		int randomGeoLoc =(int) (Math.random() * 10);
		this.genericGeoSelected = this.genericGeographicPlaces[randomGeoLoc];
	}
	private void generateTown() {
		this.currentTown = this.townList.get(0);
	}

	/**
	 *  Getters and setters
	 * @return
	 */
	public String getGeoLocation() {
		return this.genericGeoSelected;
	}
	public ArrayList<String> getTownList() {
		return this.townList;
	}

	public void setLocationList(ArrayList<String> locationList) {
		this.townList = locationList;
	}

	public String getCurrentTown() {
		return this.currentTown;
	}

	public void setCurrentTown(String currentLocation) {
		this.currentTown = currentLocation;
	}
	
}
