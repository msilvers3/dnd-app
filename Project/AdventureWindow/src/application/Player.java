package application;
import java.lang.Math;
/**
 * this is the class that determines player object stats and levels.
 * @author TeamOmega
 *
 */
public class Player extends Entity implements AttackInterface {
	private int base = 5;
	private int xp;
	private double maxXp;
	private int checkForAttack;
	private int damageModifier;
	private int levelsUp;
	private boolean canLevelUp;
	
	private Weapon weapon;
	
	public Player(String name, Weapon weapon) {
		super(name);
		this.weapon = weapon;
		this.xp = 0;
		this.maxXp = 100;
		this.checkForAttack = 10;
		this.damageModifier = weapon.getTotalDamage();
		this.canLevelUp = false;
	}
	
	
	
	
	
	public void calculateCheckForAttack(int level) {
		if (level<7) {
			this.checkForAttack = 12;
		}
		else if (level<14) {
			this.checkForAttack = 13;
		}
		else if (level<27) {
			this.checkForAttack = 14;
		}
		else if (level<40) {
			this.checkForAttack = 15;
		}
		else {
			this.checkForAttack = 16;
		}
	}

	public int getCheckForAttack() {
		return checkForAttack;
	}

	public void setCheckForAttack(int checkForAttack) {
		this.checkForAttack = checkForAttack;
	}

	public void addXp(int xp) {
		this.levelsUp = 0;
		int currentXP = this.xp;
		while(true) {
			if (currentXP + xp >= this.maxXp) {
				xp = (int) ((currentXP + xp) - this.maxXp);
				currentXP = 0;
				this.maxXp *= 1.10;	
				this.levelsUp += 1;
			}
			else {
				if(this.levelsUp != 0) {
					this.xp = xp;
				}
				else {
					this.xp += xp;
				}
				break;
			}
			
		}
		if (this.levelsUp != 0) {
			this.canLevelUp = true;
		}
		
	}
	
	public boolean canLevelUp() {
		return this.canLevelUp;
	}
	
	public void setCanLevelUp(boolean statement) {
		this.canLevelUp = statement;
	}
	
	public int getLevelsUp() {
		return this.levelsUp;
	}
	
	public double getMaxXp() {
		return this.maxXp;
	}
	public void setMaxXp(double maxXp) {
		this.maxXp = maxXp;
	}
	
	
	
	public int getXp() {
		return this.xp;
	}
	public void setXp(int xp) {
		this.xp = xp;
	}
	
	
	
	public void restPlayer() {
		setHealth(getMaxHealth());
	}

	public void levelUp(int selector) {
		super.setLevel(this.getLevel()+1);
		super.setHealth(super.getMaxHealth()+25.0);
		super.setMaxHealth(super.getHealth());
	}

	
	public void addStrength(int point) {
		super.setStrength(super.getStrength()+point);
	}
	
	public void addIntelligence(int point) {
		super.setIntelligence(super.getIntelligence()+point);
	}
	
	public void addMagicPower(int point) {
		super.setMagicPower(super.getMagicPower()+point);
	}
	
	public void addLuck(int point) {
		super.setLuck(super.getLuck()+point);
	}
	
	
	
	

	@Override
	public int rollToHit() {
		int luck = getLuck();
		if (getLuck()>75) {
			luck = 75;
		}
		int randomNum = (int)(Math.random()*20)+(int)((getLuck()/100)*10);
		return randomNum;
	}

	public Weapon getWeapon() {
		return this.weapon;
	}
	
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}



	@Override
	public double attack() {
		double fraction = (Math.random()*100)/100;
		int power = (int)((this.getStrength() + this.getIntelligence() + this.getMagicPower())*fraction);
		double totalAttack = this.base + this.damageModifier + power ;
		return totalAttack;
	}
	
	@Override
	public String toString() {
		return String.format(
				"Name: %s\n"
				+"%s\n\n"
				+"Base Dmg: %d\n"
				+"Attack Check: %d\n"
				+"Damage Modifier: %d\n"
				+"Weapon: %s\n"
				+ "XP: %d / %.0f",
				getName(),
				super.toString(),
				this.base,
				this.checkForAttack,
				this.damageModifier,
				this.weapon.getItemName(),
				this.xp,
				this.maxXp);
	}

	
	
	
	
	
}
