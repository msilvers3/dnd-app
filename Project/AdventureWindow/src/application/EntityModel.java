package application;
import java.util.ArrayList;
/**
 * model class for the Entity class
 * @author TeamOmega
 *
 */
public class EntityModel {
	ArrayList<Entity> entities;
	
	
	public EntityModel() {
		this.entities = new ArrayList<>();
	}
	
	public void addEntity(Entity entity) {
		this.entities.add(entity);
	}
	
	
	public Entity getEntity(String name) {
		
		for (Entity entity:entities) {
			if (entity.getName().equals(name)) {
				return entity;
			}
		
		}
		return new Entity("Null");
	}
	@Override
	public String toString() {
		String out = "";
		for(Entity entity:entities) {
			out+=entity.toString();
		}
		return String.format("%s", out);
	}
	public ArrayList<Entity> getArrayListEntities()
	{
		return this.entities;
	}
	
}
