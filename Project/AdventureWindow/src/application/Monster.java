package application;
import java.lang.Math;
/**
 * this class will generate a monster object and use the attack interface so it can interact with the player object through the combat controller
 * @author TeamOmega
 *
 */
public class Monster extends Entity implements AttackInterface {
	private double abilityPoints;
	private String tier;
	private int xpDropped;
	private boolean dead;
	private int checkAttack;
	
	
	public Monster(String name,String tier) {
		super(name);
		this.tier = tier;
		this.abilityPoints = calculateAbilityPoints(tier);
		this.dead = false;
		this.checkAttack = 10+ (int)(Math.random()*5);
	}
	
	
	private double calculateHealth(int level) {
		double extraHealth = 0;
		for (int i = 0; i<level;i++) {
			extraHealth+=25;
		}
		return extraHealth;
	}
	
	private double calculateAbilityPoints(String tier) {
		int level;
		if(tier.equals("E")) {
			level = 2 + (int)(Math.random()*3);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 25+(int)(Math.random()*10);
			
		}
		else if(tier.equals("D")) {
			level = 5 + (int)(Math.random()*5);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 50+(int)(Math.random()*20);
		}
		else if(tier.equals("C")) {
			level = 10 + (int)(Math.random()*10);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 100+(int)(Math.random()*40);
		}
		else if(tier.equals("B")) {
			level = 20 + (int)(Math.random()*20);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 200+(int)(Math.random()*80);
		}
		else if(tier.equals("A")) {
			level = 40 + (int)(Math.random()*30);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 400+(int)(Math.random()*160);
		}
		else if(tier.equals("S")) {
			level = 70 + (int)(Math.random()*30);
			super.setLevel(level);
			super.setHealth(super.getHealth()+calculateHealth(level));
			this.xpDropped = 1000+(int)(Math.random()*500);
		}
		else {
			level = 1;
			super.setLevel(level);
		}
		

		for(int i=1;i<=level;i++) {
			double randomNum = Math.random()*100;
			int addPoint = i-(i-1);
			if(i==level) {
				
			}
			else if (level>70){
				if(randomNum<26) {
					focusMagic(addPoint);
					focusMagic(addPoint);
				}
				else if(randomNum<51) {
					focusStrength(addPoint);
					focusStrength(addPoint);
				}
				else if(randomNum<76) {
					focusIntelligence(addPoint);
					focusIntelligence(addPoint);
				}
				else {
					focusLuck(addPoint);					
				}
			}
			else {
				if(randomNum<26) {
					focusMagic(addPoint);
				}
				else if(randomNum<51) {
					focusStrength(addPoint);
				}
				else if(randomNum<76) {
					focusIntelligence(addPoint);
				}
				else {
					focusLuck(addPoint);
				}
			}
		}
		return getAP();
		
	}
	
	private void focusMagic(double level) {
		setMagicPower(getMagicPower()+level);
	}
	private void focusStrength(double level) {
		setStrength(getStrength()+level);
	}
	private void focusIntelligence(double level) {
		setIntelligence(getIntelligence()+level);
	}
	private void focusLuck(double level) {
		setLuck(getLuck()+(int)level);
	}
	
	public String getTier() {
		return this.tier;
	}
	public int getAttackCheck() {
		return this.checkAttack;
	}
	
	public void setDead(boolean status) {
		this.dead = status;
	}
	public boolean checkIfDead() {
		return this.dead;
	}
	
	@Override
	public int rollToHit() {
		int randomNum = (int)(Math.random()*10)+  (int)(Math.random()*10);
		return randomNum;
	}


	@Override
	public double attack() {
		double totalAttack = ((super.getStrength() + super.getMagicPower() + super.getIntelligence())*(Math.random()+.1));
//		System.out.println(String.format("Total Monster Atk: %.2f\n", totalAttack));
		return totalAttack;
	}
	
	public int getXPDropped() {
		return this.xpDropped;
	}
	@Override
	public String toString() {

		return String.format(
				"Monster Name: %s\n"
				+ "Tier: %s\n"
				+ "%s\n"
				+"Total Monster AP: %.2f\n",
				super.getName(),
				getTier(),
				super.toString(),
				this.abilityPoints); 
	}




	
	

}
