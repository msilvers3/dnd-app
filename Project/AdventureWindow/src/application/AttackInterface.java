package application;
/**
 * this is the interface that will be called from the combat controller to handle the calculations needed to determine 
 * if the attack hits or not, and what damage will be applied to the monster and the player
 * @author TeamOmega
 *
 */
public interface AttackInterface {
	int rollToHit();
	double attack();
}
