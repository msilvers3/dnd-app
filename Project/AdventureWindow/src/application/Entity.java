package application;
/**
 * the entity class that handles making entities throughout the application
 * @author TeamOmega
 *
 */
public class Entity {
	private String name;
	private int level;
	private double maxHealth;
	private double health;
	private double strength;
	private double magicPower;
	private double intelligence;
	private int luck;
	private boolean dead;
	
	
	
	//Constructor for name with base stats
	public Entity(String name) {
		this.name = name;
		this.level = 1;
		this.maxHealth = 100;
		this.health = 100;
		this.strength = 10;
		this.magicPower = 7;
		this.intelligence = 8;
		this.luck = 4;
		this.dead = false;
	}
	//Constructor for Monster generation
	public Entity(String name, int level) {
		this.name = name;
		this.level = level;
		this.maxHealth = 100;
		this.health = 100;
		this.strength = 10;
		this.magicPower = 7;
		this.intelligence = 8;
		this.luck = 4;
		this.dead = false;
	}
	// Constructor for full editing
	public Entity(String name,
			int level,
			double maxHealth,
			double health,
			double strength,
			double magicPower,
			double intelligence,
			int luck) {
		this.name = name;
		this.level = level;
		this.maxHealth = maxHealth;
		this.strength = strength;
		this.magicPower = magicPower;
		this.intelligence = intelligence;
		this.luck = luck;
		this.dead = false;
	}
	
	public void setDead(boolean status) {
		this.dead = status;
	}
	
	public boolean getDeadStatus() {
		return this.dead;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLevel() {
		return this.level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public double getMaxHealth() {
		return this.maxHealth;
	}
	public void setMaxHealth(double maxHealth) {
		this.maxHealth = maxHealth;
	}
	public double getHealth() {
		return this.health;
	}
	public void setHealth(double health) {
		this.health = health;
	}
	public double getStrength() {
		return this.strength;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
	public double getMagicPower() {
		return this.magicPower;
	}
	public void setMagicPower(double magicPower) {
		this.magicPower = magicPower;
	}
	public double getIntelligence() {
		return this.intelligence;
	}
	public void setIntelligence(double intelligence) {
		this.intelligence = intelligence;
	}
	public int getLuck() {
		return this.luck;
	}
	public void setLuck(int luck) {
		this.luck = luck;
	}
	public double getAP() {
//		int totalAP = 
		double totalAP = getLuck()+getIntelligence()+getMagicPower()+getStrength();
		return totalAP;
	}
	@Override
	public String toString() {
		String status;
		if (this.dead) {
			status = "Dead";
		}
		else {
			status = "Alive";
		}
		return String.format(
				"Stats\n"
				+ "Level: %d\n"
				+ "HP: %.2f\n"
				+ "Strength: %.2f\n"
				+ "Magic: %.2f\n"
				+ "Wisdom: %.2f\n"
				+ "Luck: %d\n"
				+ "Status: %s", 
				this.level,
				this.health,
				this.strength,
				this.magicPower,
				this.intelligence,
				this.luck,
				status);
	}
}
