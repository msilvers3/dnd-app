package application;
import java.util.Scanner;
import java.lang.Math;
public class TestEntity {
/**
 * this class tests whether the entity made is valid
 * @param args
 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Weapon staff = new Weapon("Apprentice Staff", "Deals magic damage",2,1,5,3);
		Weapon mace = new Weapon("Small Mace","Deals blunt damage",5,0,2,1);
		Player player1 = new Player("Alex",staff);
		player1.setLevel(1);
		
		WeaponModel weapons = new WeaponModel();
		weapons.addWeapon(staff);
		weapons.addWeapon(mace);
		EventModel events = new EventModel(); 
		Location testlocation = new Location();
		int combat = 1;
		int discovery = 2;
		
		while(true) {
			int eventchoice = (int)(Math.random()*3);
			System.out.println("You're walking on a path and come to a fork, which path do you take?\n");
			System.out.println("1. The Left.\n2. The Right");
			int decision = input.nextInt();
			if (eventchoice == combat) {
				events.addEvent(new CombatEvent(player1,testlocation));
				System.out.println(String.format(
						"You've entered Combat with %s.\n"
						+ "Enter Choice\n"
						+ "Type\n1. to Attack\n"
						+ "2. to Rest\n"
						+ "3. to Run Away\n",
						((CombatEvent)events.getLastEvent()).getMonster().getName()));
				
				while (!((CombatEvent)events.getLastEvent()).getMonster().checkIfDead()) {
					System.out.println(String.format(
									"Monster Name: %s\n"
									+ "Monster Tier: %s\n"
									+ "Monster Health: %.2f\n"
									+ "Your Health: %.2f\n"
									+ "\n"
									+ "",((CombatEvent)events.getLastEvent()).getMonster().getName(),
											((CombatEvent)events.getLastEvent()).getMonster().getTier(),
											((CombatEvent)events.getLastEvent()).getMonster().getHealth(),
											((CombatEvent)events.getLastEvent()).getPlayer().getHealth()));
					int choice = input.nextInt();
						
					if (choice==1) {
						System.out.println(((CombatEvent)events.getLastEvent()).getAttackMessage());
					}
					else if(choice ==2) {
						((CombatEvent)events.getLastEvent()).getPlayer().setHealth(((CombatEvent)events.getLastEvent()).getPlayer().getMaxHealth());
						System.out.println("You rested for a moment to heal\n");
						System.out.printf("%s\n\n", ((CombatEvent)events.getLastEvent()).getPlayer());
					}
					else if (choice ==3 ) {
						int chanceToRun = (int)(Math.random()*20);
						if (((CombatEvent)events.getLastEvent()).getPlayer().getLuck() > chanceToRun) {
							break;
						}
					}
					else {
						System.out.println("Nothing was chosen.\n");
					}
					
					if (((CombatEvent)events.getLastEvent()).getMonster().getDeadStatus()) {
						System.out.println(String.format("You defeated %s\n"
								+ "You gained %d", 
								((CombatEvent)events.getLastEvent()).getMonster().getName(),
								((CombatEvent)events.getLastEvent()).getMonster().getXPDropped()));
						
					}
				}
			}
			else if (eventchoice == discovery) {
				events.addEvent(new DiscoveryEvent(player1, testlocation));
				System.out.println(((DiscoveryEvent)events.getLastEvent()).getDiscoveryEventMessage());
			}
			else {
				System.out.println("You closed your eyes and fell asleep.");
				player1.restPlayer();
			}
		}
		
	}

}
