package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
/**
 * this class handles the leveling of your character and how you will spec it out, applies it to your player object
 * @author TeamOmega
 *
 */
public class LevelUpController {

// Create your objects but not initialize them
	Player player;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	CombatEvent combat;
// Create your controllers but not initialize them
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	WeaponSelectController wpnCon;
	private int levelUpPoints;
	private int strPoints;
	private int magPoints;
	private int intPoints;
	private int lckPoints;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button intelligenceAddButton;

    @FXML
    private Button intelligenceMinusButton;

    @FXML
    private Label intelligenceNumLabel;

    @FXML
    private Label leftNumLabel;

    @FXML
    private Button luckAddButton;

    @FXML
    private Button luckMinusButton;

    @FXML
    private Label luckNumLabel;

    @FXML
    private Button magicAddButton;

    @FXML
    private Button magicMinusButton;

    @FXML
    private Label magicNumLabel;

    @FXML
    private Label playerNameLabel;

    @FXML
    private Button strengthAddButton;

    @FXML
    private Button strengthMinusButton;

    @FXML
    private Label strengthNumLabel;

    @FXML
    private Button submitButton;

    @FXML
    void handleIntelligenceAdd(ActionEvent event) {
    	if (this.levelUpPoints == 0) {
    		System.out.println("No more points to use.");
    	}
    	else if (this.levelUpPoints - 1 != 0 || this.levelUpPoints -1 == 0) {
    		player.addIntelligence(1);
    		this.levelUpPoints -=1;
    		this.intPoints +=1;
    		this.intelligenceNumLabel.setText(String.format("%d", (int)player.getIntelligence()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    	
    }

    @FXML
    void handleIntelligenceMinusButton(ActionEvent event) {
    	if (this.intPoints != 0) {
    		player.setIntelligence(player.getIntelligence()-1);
    		this.intPoints -=1;
    		this.levelUpPoints +=1;
    		this.intelligenceNumLabel.setText(String.format("%d", (int)player.getIntelligence()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    }

    @FXML
    void handleLuckAdd(ActionEvent event) {
    	if (this.levelUpPoints == 0) {
    		System.out.println("No more points to use.");
    	}
    	else if (this.levelUpPoints - 1 != 0 || this.levelUpPoints -1 == 0) {
    		player.addLuck(1);
    		this.levelUpPoints -=1;
    		this.lckPoints +=1;
    		this.luckNumLabel.setText(String.format("%d", (int)player.getLuck()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    	
    }

    @FXML
    void handleLuckMinusButton(ActionEvent event) {
    	if (this.lckPoints != 0) {
    		player.setLuck(player.getLuck()-1);
    		this.lckPoints -=1;
    		this.levelUpPoints +=1;
    		this.luckNumLabel.setText(String.format("%d", (int)player.getLuck()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    }

    @FXML
    void handleMagicAdd(ActionEvent event) {
    	if (this.levelUpPoints == 0) {
    		System.out.println("No more points to use.");
    	}
    	else if (this.levelUpPoints - 1 != 0 || this.levelUpPoints -1 == 0) {
    		player.addMagicPower(1);
    		this.levelUpPoints -=1;
    		this.magPoints +=1;
    		this.magicNumLabel.setText(String.format("%d", (int)player.getMagicPower()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    	
    }

    @FXML
    void handleMagicMinusButton(ActionEvent event) {
    	if (this.magPoints != 0) {
    		player.setMagicPower(player.getMagicPower()-1);
    		this.magPoints -=1;
    		this.levelUpPoints +=1;
    		this.magicNumLabel.setText(String.format("%d", (int)player.getMagicPower()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    }

    @FXML
    void handleStrengthAdd(ActionEvent event) {
    	if (this.levelUpPoints == 0) {
    		System.out.println("No more points to use.");
    	}
    	else if (this.levelUpPoints - 1 != 0 || this.levelUpPoints -1 == 0) {
    		player.addStrength(1);
    		this.levelUpPoints -=1;
    		this.strPoints+=1;
    		this.strengthNumLabel.setText(String.format("%d", (int)player.getStrength()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    		System.out.println(player.toString());
    	}
    	
    }

    @FXML
    void handleStrengthMinusButton(ActionEvent event) {
    	if (this.strPoints != 0) {
    		player.setStrength(player.getStrength()-1);
    		this.strPoints -=1;
    		this.levelUpPoints +=1;
    		this.strengthNumLabel.setText(String.format("%d", (int)player.getStrength()));
    		this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
    	}
    }

    @FXML
    void handleSubmitButton(ActionEvent event) throws IOException {
    	player.levelUp(this.strPoints+this.intPoints+this.lckPoints);
    	if (this.levelUpPoints == 0) {
    		player.setCanLevelUp(false);
    	}
    	
    	backToPath(event);
    }

    @FXML
    void initialize() {
        assert intelligenceAddButton != null : "fx:id=\"intelligenceAddButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert intelligenceMinusButton != null : "fx:id=\"intelligenceMinusButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert intelligenceNumLabel != null : "fx:id=\"intelligenceNumLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert leftNumLabel != null : "fx:id=\"leftNumLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert luckAddButton != null : "fx:id=\"luckAddButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert luckMinusButton != null : "fx:id=\"luckMinusButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert luckNumLabel != null : "fx:id=\"luckNumLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert magicAddButton != null : "fx:id=\"magicAddButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert magicMinusButton != null : "fx:id=\"magicMinusButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert magicNumLabel != null : "fx:id=\"magicNumLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert playerNameLabel != null : "fx:id=\"playerNameLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert strengthAddButton != null : "fx:id=\"strengthAddButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert strengthMinusButton != null : "fx:id=\"strengthMinusButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert strengthNumLabel != null : "fx:id=\"strengthNumLabel\" was not injected: check your FXML file 'LevelUp.fxml'.";
        assert submitButton != null : "fx:id=\"submitButton\" was not injected: check your FXML file 'LevelUp.fxml'.";
        this.levelUpPoints = player.getLevelsUp();
        this.strengthNumLabel.setText(String.format("%d", (int)player.getStrength()));
        this.intelligenceNumLabel.setText(String.format("%d", (int)player.getIntelligence()));
        this.magicNumLabel.setText(String.format("%d", (int)player.getMagicPower()));
        this.luckNumLabel.setText(String.format("%d", (int)player.getLuck()));
        this.leftNumLabel.setText(String.format("%d", this.levelUpPoints));
        this.strPoints = 0;
        this.magPoints = 0;
        this.intPoints = 0;
        this.lckPoints = 0;
      
    }

    
    public void setMainObjects(Player player, Location location, EventModel events, EntityModel entities, ItemModel items, WeaponModel weapons) {
    	this.player = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.combat = (CombatEvent)this.events.getLastEvent();
    }
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.wpnCon = wpnCon;
    }
    
    private void backToPath(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("Path.fxml"));
		
    	// Set the path controller objects
    	pathController.setControllers(mmController, pathController, combatController, wpnCon);
    	pathController.setMainObjects(this.player, locObject, events, entities, items, weapons);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(pathController);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 600,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
}
