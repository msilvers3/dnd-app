package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
/**
 * this class determines what happens on which path you choose to venture on and what you will be presented with
 * @author TeamOmega
 *
 */
public class PathController {
	// Create your objects but not initialize them
	Player player1;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	Thread thread1;
	Thread thread2;
	Thread thread3;
	Sound8BitAdventure sound8BitAdv;
	SoundRetroForest soundRetroForest;
	SoundBossBattle soundBossBattle;
	
	Alert alert = new Alert(AlertType.INFORMATION);
	// Create your controllers but not initialize them
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	WeaponSelectController wpnCon;
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;


    @FXML
    private TextArea playerStatsTextArea;
    @FXML
    private Button Left;

    @FXML
    private Button Right;

    @FXML
    private TextArea informationTextArea;

    @FXML
    private ImageView image;

    @FXML
    private Label pathLabel;

    @FXML
    private Button restButton;
    
    @FXML
    private Button viewDefeatedButton;

    @FXML
    private Button viewInventoryButton;

    @FXML
    private Button viewWeaponsButton;

    
    @FXML
    void handleRestButton(ActionEvent event) {
    	informationTextArea.setText("");
    	this.playerStatsTextArea.setText("");
    	if (player1.getHealth() != player1.getMaxHealth()) {
    		informationTextArea.setText(String.format("Healed for\n%.0f",player1.getMaxHealth()-player1.getHealth()));
    		player1.setHealth(player1.getMaxHealth());
    		this.setPlayerTextArea();
    	}
    	else {
    		this.informationTextArea.setText("You are already at full health!");
    		this.setPlayerTextArea();
    		
    	}
    	this.setPlayerTextArea();
    }
    
    
    
    @FXML
    void handleViewDefeatedButton(ActionEvent event) {
    	informationTextArea.setText("");
    	int count = 0;
    	for(Entity entity: this.entities.getArrayListEntities())
    	{
    		if (!entity.getDeadStatus()) {
    			informationTextArea.appendText(String.format("Monster #%d\n%s\nLevel: %d\n\n", count,entity.getName(), entity.getLevel()));
    			count++;
    		}
		}
    	if (count==0) {
    		informationTextArea.setText("No Monsters Defeated");
    	}
    }

    @FXML
    void handleViewInventory(ActionEvent event) {
    	this.informationTextArea.setText("");
    	int count = 1;
    	for (Item item:this.items.getItemArrayList()) {
    		this.informationTextArea.appendText(
    				String.format(
    						"Item #%d\n%s\nDescription\n%s\n\n", 
    						count,
    						item.getItemName(),
    						item.getDescription()));
    		count++;
    	}
    	if (count==1) {
    		informationTextArea.setText("Nothing in inventory");
    	}
    }

    @FXML
    void handleViewWeaponsButton(ActionEvent event) throws IOException {
    	goToWeaponSelect(event);
    }

    @FXML
    void leftHandle(ActionEvent event) throws IOException
    {
 
    	int randomNum = (int)(Math.random()*1000);
       	if (randomNum<500) {
    		events.addEvent(new DiscoveryEvent(player1, locObject));
    		pathLabel.setText(((DiscoveryEvent)events.getLastEvent()).getDiscoveryEventMessage());
    		items.addItem(((DiscoveryEvent)events.getLastEvent()).getItem());
    		String desc;
    	
    		if (((DiscoveryEvent)events.getLastEvent()).getItem().getItemName().contains("health potion")) {
    			player1.setHealth(this.player1.getMaxHealth());
    		}
    		else if (((DiscoveryEvent)events.getLastEvent()).getItem().getItemName().contains("scroll of strength")) {
    			player1.addStrength(1);
    		}
    		else if (((DiscoveryEvent)events.getLastEvent()).getItem().getItemName().contains("scroll of knowledge")) {
    			player1.addIntelligence(1);
    		}
    		else if (((DiscoveryEvent)events.getLastEvent()).getItem().getItemName().contains("scroll of magic")) {
    			player1.addMagicPower(1);
    		}
    		else if (((DiscoveryEvent)events.getLastEvent()).getitemNameSelected().contains("staff") ||
    				((DiscoveryEvent)events.getLastEvent()).getitemNameSelected().contains("sword") ||
    				((DiscoveryEvent)events.getLastEvent()).getitemNameSelected().contains("club") ||
    				((DiscoveryEvent)events.getLastEvent()).getitemNameSelected().contains("gunmace")) {
    			desc = ((DiscoveryEvent)events.getLastEvent()).getItem().getDescription();
    			Weapon weapon = (Weapon)((DiscoveryEvent)events.getLastEvent()).getItem();
    			this.weapons.addWeapon(weapon);
    		}
    	}
    	else if (randomNum>500) {
    		
    		generateCombatEvent(event);
    	}
       	this.setPlayerTextArea();
    }

    @FXML
    void rightHandle(ActionEvent event) throws IOException
    {
    	leftHandle(event);
    }

    @FXML
    void initialize() {
        assert Left != null : "fx:id=\"Left\" was not injected: check your FXML file 'Path.fxml'.";
        assert Right != null : "fx:id=\"Right\" was not injected: check your FXML file 'Path.fxml'.";
        assert informationTextArea != null : "fx:id=\"informationTextArea\" was not injected: check your FXML file 'Path.fxml'.";
        assert image != null : "fx:id=\"image\" was not injected: check your FXML file 'Path.fxml'.";
        assert pathLabel != null : "fx:id=\"pathLabel\" was not injected: check your FXML file 'Path.fxml'.";
        assert viewDefeatedButton != null : "fx:id=\"viewDefeatedButton\" was not injected: check your FXML file 'Path.fxml'.";
        assert viewInventoryButton != null : "fx:id=\"viewInventoryButton\" was not injected: check your FXML file 'Path.fxml'.";
        assert viewWeaponsButton != null : "fx:id=\"viewWeaponsButton\" was not injected: check your FXML file 'Path.fxml'.";
       	// UnComment to face Boss
//        this.player1.setHealth(2500);
//        this.player1.setMaxHealth(2500);
//    	this.player1.setLevel(71);
//    	this.player1.setStrength(35);
//    	this.player1.setMagicPower(10);
//    	this.player1.setIntelligence(20);
//    	this.player1.setLuck(5);
        
        
        Sound8BitAdventure sound8BitAdv = new Sound8BitAdventure();
		SoundRetroForest soundRetroForest = new SoundRetroForest();
		SoundBossBattle soundBossBattle = new SoundBossBattle();
		
		thread1 = new Thread(sound8BitAdv, "Adventure Start Running");
		thread2 = new Thread(soundRetroForest, "Path Sound Running");
		thread3 = new Thread(soundBossBattle, "Boss Sound Running");
		thread2.start();
        playerStatsTextArea.setText(String.format(
					"%s\n"
					+ "Level: %d\n"
					+ "Health: %.2f\n"
					+ "Str: %.0f\n"
					+ "Int: %.0f\n"
					+ "Mag: %.0f\n"
					+ "Luck: %d\n"
					+ "XP\n %d / %.0f\n\n"
					+ "Weapon Info\n"
					+ "Name: %s\n"
					+ "Blunt: %d\n"
					+ "Pierce: %d\n"
					+ "Magic: %d\n",
					player1.getName(),
					player1.getLevel(),
					player1.getHealth(),
					player1.getStrength(),
					player1.getIntelligence(),
					player1.getMagicPower(),
					player1.getLuck(),
					player1.getXp(),
					player1.getMaxXp(),
					player1.getWeapon().getItemName(),
					player1.getWeapon().getBluntDamage(),
					player1.getWeapon().getPierceDamage(),
					player1.getWeapon().getMagicDamage()));
    }

    public void generateCombatEvent(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("CombatWindow.fxml"));

    	
    	if (player1.getLevel() >70) {
    		thread2.interrupt();
    		thread3.start();
    	}
    	// Generate a new combat event in the events model
    	events.addEvent(new CombatEvent(player1, locObject));
    	// Set the path controller objects
    	combatController.setControllers(mmController, pathController, combatController, wpnCon);
    	combatController.setMainObjects(player1, 
    			locObject, 
    			events, 
    			entities, 
    			items, 
    			weapons, 
    			thread1,
    			thread2,
    			thread3,
    			sound8BitAdv, 
    			soundRetroForest,
    			soundBossBattle);
    	
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(combatController);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 500,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    
    public void goToWeaponSelect(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("WeaponSelectWindow.fxml"));

    	// Set the path controller objects
    	wpnCon.setControllers(mmController, pathController, combatController, wpnCon);
    	wpnCon.setMainObjects(player1, locObject, events, entities, items, weapons);
    	
    	
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(wpnCon);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 600,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    
 // initialize the objects
    public void setMainObjects(Player player, 
    		Location location, 
    		EventModel events, 
    		EntityModel entities, 
    		ItemModel items, 
    		WeaponModel weapons,
    		Thread thread1,
    		Thread thread2,
    		Thread thread3,
    		Sound8BitAdventure s8ba,
    		SoundRetroForest srf,
    		SoundBossBattle sbb) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.thread1 = thread1;
    	this.thread2 = thread2;
    	this.thread3 = thread3;
    	this.sound8BitAdv = s8ba;
    	this.soundRetroForest = srf;
    	this.soundBossBattle = sbb;
    }
    
    // initialize the controllers
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.wpnCon = wpnCon;
    }
    
    
    public void setMainObjects(Player player, Location location, EventModel events, EntityModel entities, ItemModel items, WeaponModel weapons) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    }
    private void setPlayerTextArea() {
    	playerStatsTextArea.setText(String.format(
				"%s\n"
				+ "Level: %d\n"
				+ "Health: %.2f\n"
				+ "Str: %.0f\n"
				+ "Int: %.0f\n"
				+ "Mag: %.0f\n"
				+ "Luck: %d\n"
				+ "XP\n %d / %.0f\n\n"
				+ "Weapon Info\n"
				+ "Name: %s\n"
				+ "Blunt: %d\n"
				+ "Pierce: %d\n"
				+ "Magic: %d\n",
				player1.getName(),
				player1.getLevel(),
				player1.getHealth(),
				player1.getStrength(),
				player1.getIntelligence(),
				player1.getMagicPower(),
				player1.getLuck(),
				player1.getXp(),
				player1.getMaxXp(),
				player1.getWeapon().getItemName(),
				player1.getWeapon().getBluntDamage(),
				player1.getWeapon().getPierceDamage(),
				player1.getWeapon().getMagicDamage()));
    }
}
