package application;

import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
/** 
 * the combat controller class will handle the combat window events
 * @author TeamOmega
 *
 */
public class CombatController {

	// Create your objects but not initialize them
	Player player1;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	CombatEvent combat;
	Alert alert = new Alert(AlertType.INFORMATION);
	Alert runaway = new Alert(AlertType.INFORMATION);
	Alert dead = new Alert(AlertType.INFORMATION);
	// Create your controllers
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	LevelUpController lvlUpCon;
	WeaponSelectController wpnCon;
	Thread thread1;
	Thread thread2;
	Thread thread3;
	Sound8BitAdventure sound8BitAdv;
	SoundRetroForest soundRetroForest;
	SoundBossBattle soundBossBattle;

//	File sound = new File("sound/Recording.m4a");
//	BufferedInputStream is = new BufferedInputStream(this.getClass().getResourceAsStream("sound/Recording.wav"));
	Long currentFrame;
	Clip clip;
	String status;
	AudioInputStream audioInputS;
	String filePath = "sound/runAwaySound.wav";
	private void SimpleAudioPlayer() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		this.audioInputS = AudioSystem.getAudioInputStream(new File(getClass().getResource(filePath).getFile()));
		clip = AudioSystem.getClip();
		clip.open(audioInputS);
		clip.loop(0);

	}
	Clip deadnoise;
	String death;
	AudioInputStream audioInputdead;
	String deadfilePath = "sound/Dead.wav";
	private void DeadAudioPlayer() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		this.audioInputS = AudioSystem.getAudioInputStream(new File(getClass().getResource(deadfilePath).getFile()));
		clip = AudioSystem.getClip();
		clip.open(audioInputS);
		clip.loop(0);

	}
	/**
	 * below is all the ways we handle interactions within the GUI of the application that deals with combat
	 */
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button Attack;

    @FXML
    private Button Rest;

    @FXML
    private TextArea enemyStats;

    @FXML
    private TextArea history;

    @FXML
    private ImageView image;

    @FXML
    private TextArea playerStats;

    @FXML
    private Button runAwayButton;

    private Label[] labels;
	private int currentLabel;
	
    @FXML
    void attackHandle(ActionEvent event) throws IOException, UnsupportedAudioFileException, LineUnavailableException
    {
    	String currentText = history.getText();
    	history.setText(combat.getAttackMessage() + "\n\n" + currentText);
    	this.setPlayerStatsTxtArea();
        enemyStats.setText(String.format(
        		"%s\n"
        		+ "Tier: %s\n"
        		+ "Level: %d\n"
        		+ "Health: %.2f",
        		combat.getMonster().getName(),
        		combat.getMonster().getTier(),
        		combat.getMonster().getLevel(),
        		combat.getMonster().getHealth()));


        if( combat.getMonster().checkIfDead()) {
        	entities.addEntity(combat.getMonster());
        	combat.getPlayer().addXp(combat.getMonster().getXPDropped());
        	player1 = combat.getPlayer();
        	alert.showAndWait();
        	if (player1.canLevelUp()) {
        		sendToPopup(event);
        	}
        	else
        	{
        		backToPath(event);
        	}
 	}
        if(combat.getPlayer().getDeadStatus())
		{
        	DeadAudioPlayer();
        	dead.showAndWait();    
        	combat.getPlayer().setDead(false);
        	combat.getPlayer().setHealth(combat.getPlayer().getMaxHealth());
        	backToMain(event);
		}
    }

	@FXML
    void handleRunAwayButton(ActionEvent event) throws IOException, UnsupportedAudioFileException, LineUnavailableException
	{
		
		
		int randomNum = (int)(Math.random()*100);
		if (randomNum+player1.getLuck() > 50 ) {
			SimpleAudioPlayer();
            
			runaway.showAndWait();
			backToPath(event);
		}
//		else if (combat.getMonster().checkIfDead()) {
//			backToPath(event);
//		}
		else {
			String currentText = history.getText();
			double attackDamage = combat.getMonster().attack();
			if (randomNum+player1.getLuck()>25) {
				if (player1.getHealth()-attackDamage <= 0) {
					DeadAudioPlayer();
					dead.setContentText(
						String.format(
							"%s hit you while you were trying to run away!\n"
							+ "You died!",
							combat.getMonster().getName()));
		        	dead.showAndWait();        	
		        	backToMain(event);
				}
				history.setText(
						String.format(
									"%s hit you while you were trying to run away!\n"
									+ "You lost %.0f Health!!\n\n%s",
									combat.getMonster().getName(),
									attackDamage,
									currentText));
				this.setPlayerStatsTxtArea();
			}
			else {
				history.setText(
						String.format(
						"%s has their eyes on you. You couldn't run away!\n\n%s", 
						combat.getMonster().getName(),
						currentText));
			}
			
		}

	}
    @FXML
    void restHandle(ActionEvent event) 
    {
    	int count = 1;
		if(combat.getPlayer().getHealth() == combat.getPlayer().getMaxHealth())
		{
			String currentText = history.getText();
			history.setText(String.format("You are already at full health\n\n%s",currentText));
			return;
		}
		while (count == 1) {
			int randomNum = (int) (Math.random() * 1000);
			if (randomNum + player1.getLuck() < 500) {
				if(combat.getPlayer().getHealth() + (combat.getPlayer().getMaxHealth() * 0.10) > combat.getPlayer().getMaxHealth())
				{
					combat.getPlayer().setHealth(combat.getPlayer().getMaxHealth());
				}
				else
				{
				String currentText = history.getText();
				history.setText(String.format("You took a moment to heal.\nGained %.2f health\n\n%s", combat.getPlayer().getMaxHealth() * 0.10,currentText));
				combat.getPlayer().setHealth(combat.getPlayer().getHealth() + (combat.getPlayer().getMaxHealth() * 0.10));
				count++;
				}
			} 
			else 
			{
				String currentText = history.getText();
				history.setText(
						String.format("%s has their eyes on you. You couldn't take a moment to heal!\n\n%s", combat.getMonster().getName(),currentText));
				count++;
			}
			this.setPlayerStatsTxtArea();
		}
    }

	@FXML
    void initialize() {
        assert Attack != null : "fx:id=\"Attack\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        assert Rest != null : "fx:id=\"Rest\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        assert history != null : "fx:id=\"history\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        assert image != null : "fx:id=\"image\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        assert runAwayButton != null : "fx:id=\"runAwayButton\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        assert enemyStats != null : "fx:id=\"stats\" was not injected: check your FXML file 'CombatWindow.fxml'.";
        lvlUpCon = new LevelUpController();
        history.setText(combat.getMessage());

        enemyStats.setText(String.format(
        		"%s\n"
        		+ "Tier: %s\n"
        		+ "Level: %d\n"
        		+ "Health: %.2f",
        		combat.getMonster().getName(),
        		combat.getMonster().getTier(),
        		combat.getMonster().getLevel(),
        		combat.getMonster().getHealth()));
        alert.setTitle("Victory!");
        alert.setHeaderText("Victory!!");
        runaway.setTitle("You Ran");
        runaway.setHeaderText("You Ran!!");
        alert.setContentText(String.format("You killed the %s", combat.getMonster().getName()));
        dead.setTitle("You Died");
        dead.setHeaderText("You Died");
        dead.setContentText(String.format("You were killed by the %s", combat.getMonster().getName()));
        playerStats.setText(
    			String.format(
    					"%s\n"
    					+ "Level: %d\n"
    					+ "Health: %.2f\n"
    					+ "Weapon Info\n"
    					+ "Name: %s\n"
    					+ "Blunt: %d\n"
    					+ "Pierce: %d\n"
    					+ "Magic: %d\n"
    					+ "XP: %d / %.0f",
    			combat.getPlayer().getName(),
    			combat.getPlayer().getLevel(),
    			combat.getPlayer().getHealth(),
    			combat.getPlayer().getWeapon().getItemName(),
    			combat.getPlayer().getWeapon().getBluntDamage(),
    			combat.getPlayer().getWeapon().getPierceDamage(),
    			combat.getPlayer().getWeapon().getMagicDamage(),
    			combat.getPlayer().getXp(),
    			combat.getPlayer().getMaxXp()));
    }
    
    private void sendToPopup(ActionEvent event) throws IOException {
    	soundBossBattle.requestStop();
    	thread3.interrupt();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("LevelUp.fxml"));
    	lvlUpCon.setMainObjects(player1, locObject, events, entities, items, weapons);
    	lvlUpCon.setControllers(mmController, pathController, combatController, wpnCon);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    		
    	}
    	else {
    		loader.setController(lvlUpCon);
    		
    	}
    	
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 396, 432);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    	
    }
    
    private void backToPath(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("Path.fxml"));
		soundBossBattle.requestStop();
    	thread3.interrupt();
		
    	// Set the path controller objects
    	pathController.setControllers(mmController, pathController, combatController, wpnCon);
    	pathController.setMainObjects(player1, 
    			locObject, 
    			events, 
    			entities, 
    			items, 
    			weapons, 
    			thread1,
    			thread2,
    			thread3,
    			sound8BitAdv, 
    			soundRetroForest,
    			soundBossBattle);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(pathController);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 600,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    private void backToMain(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("MainMenuWindow.fxml"));
    	soundRetroForest.requestStop();
    	thread2.interrupt();
    	soundBossBattle.requestStop();
    	thread3.interrupt();
    	mmController.setThreads(thread1,
    							thread2,
								thread3,
								sound8BitAdv,
								soundRetroForest,
								soundBossBattle);
			if (loader.getController() != null) {
	    		System.out.println("already has controller");
	    	}
	    	else {
	    		loader.setController(mmController);
	    	}
	    	Parent parent = loader.load();
	    	Scene scene = new Scene(parent, 518,298);
	    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
	    	window.setScene(scene);
	    	window.show();
			
    }
    
    public void setMainObjects(Player player, Location location, EventModel events, EntityModel entities, ItemModel items, WeaponModel weapons) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.combat = (CombatEvent)this.events.getLastEvent();
    }
    
    public void setMainObjects(Player player, 
    		Location location, 
    		EventModel events, 
    		EntityModel entities, 
    		ItemModel items, 
    		WeaponModel weapons,
    		Thread thread1,
    		Thread thread2,
    		Thread thread3,
    		Sound8BitAdventure s8ba,
    		SoundRetroForest srf,
    		SoundBossBattle sbb) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.thread1 = thread1;
    	this.thread2 = thread2;
    	this.thread3 = thread3;
    	this.sound8BitAdv = s8ba;
    	this.soundRetroForest = srf;
    	this.soundBossBattle = sbb;
    	this.combat = (CombatEvent)this.events.getLastEvent();
    }
    
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.wpnCon = wpnCon;
    }
    
    private void setPlayerStatsTxtArea() {
    	playerStats.setText(
    			String.format(
    					"%s\n"
    					+ "Level: %d\n"
    					+ "Health: %.2f\n"
    					+ "Weapon Info\n"
    					+ "Name: %s\n"
    					+ "Blunt: %d\n"
    					+ "Pierce: %d\n"
    					+ "Magic: %d\n"
    					+ "XP: %d / %.0f",
    			combat.getPlayer().getName(),
    			combat.getPlayer().getLevel(),
    			combat.getPlayer().getHealth(),
    			combat.getPlayer().getWeapon().getItemName(),
    			combat.getPlayer().getWeapon().getBluntDamage(),
    			combat.getPlayer().getWeapon().getPierceDamage(),
    			combat.getPlayer().getWeapon().getMagicDamage(),
    			combat.getPlayer().getXp(),
    			combat.getPlayer().getMaxXp()));
    }
}
