/* this is just the main application to run the application */
package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
/**
 * This Class handles initializing the application, and making objects/models
 * @author TeamOmega
 *
 */
public class AdventureMain extends Application {
	@Override
	/**
	 * This method will make the object that is your player and send that object to other classes/interfaces when needed.
	 * it also handles adding said objects to models throughout the program
	 * @author TeamOmega
	 *
	 */
	public void start(Stage primaryStage) {
		// Create your objects/models here first
		Weapon fist = new Weapon("Fist","",1,0,0,1);
		fist.setDescription("Typically used for hand-to-hand combat.\nWould be ineffective in any other situation.");
		Player player1 = new Player("Player",fist);
		EntityModel entities = new EntityModel();
		EventModel events = new EventModel();
		ItemModel items = new ItemModel();
		Location locObject = new Location();
		WeaponModel weapons = new WeaponModel();
		Sound8BitAdventure sound8BitAdv = new Sound8BitAdventure();
		SoundRetroForest soundRetroForest = new SoundRetroForest();
		SoundBossBattle soundBossBattle = new SoundBossBattle();
		Thread thread1 = new Thread(sound8BitAdv, "Adventure Start Running");
		Thread thread2 = new Thread(soundRetroForest, "Path Sound Running");
		Thread thread3 = new Thread(soundBossBattle, "Boss Sound Running");
		
		
		// Add objects to models
//		entities.addEntity(player1);
//		events.addEvent(new DiscoveryEvent(player1, locObject));
		weapons.addWeapon(fist);
		
		// Create your controller objects
		MainMenuController mmController = new MainMenuController();
		PathController pathController = new PathController();
		CombatController combatController = new CombatController();
		SelectionController ssCon = new SelectionController();
		WeaponSelectController wpnCon = new WeaponSelectController();
//		thread1.start();
		// Set the objects for the controller  your switching to
		mmController.setControllers(mmController, pathController, combatController, ssCon, wpnCon);
		mmController.setMainObjects(player1, 
    			locObject, 
    			events, 
    			entities, 
    			items, 
    			weapons, 
    			thread1,
    			thread2,
    			thread3,
    			sound8BitAdv, 
    			soundRetroForest,
    			soundBossBattle);

		// Create the fxml loader object
		FXMLLoader loader = new FXMLLoader(getClass().getResource("MainMenuWindow.fxml"));
		try {
			
			// Set the controller for the fxml loader object 
			// Make sure in the fxml file that the fx id for
			// the controller is removed.
			loader.setController(mmController);
			AnchorPane root = loader.load();
			Scene scene = new Scene(root, 518, 298);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("Main Menu");
			primaryStage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent e) {
				sound8BitAdv.requestStop();
				soundRetroForest.requestStop();
				soundBossBattle.requestStop();
				thread1.interrupt();
				thread2.interrupt();
				thread3.interrupt();
				Platform.exit();
				System.exit(0);
			}
		});
		
	}

	
/**
 * launches application
 * @param args
 */
	public static void main(String[] args) {
		launch(args);
		
	}
}

