package application;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 * this class handles the sound file for boss fights
 * @author TeamOmega
 *
 */
public class SoundBossBattle implements Runnable {
	Clip clip;
	AudioInputStream audioInputS;
	String filePath = "sound/RedStone128-TheFinalBattle.wav";
	private boolean stopRequested = false;
	
	public synchronized void requestStop() {
		this.stopRequested = true;
	}
	
	public synchronized boolean isStopRequested() {
		return this.stopRequested;
	}
	
	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		}
		catch (InterruptedException e) {
			System.out.println("Thread asleep");
			this.stopRequested = true;
		}
	}
	@Override
	public void run() {
		while(!isStopRequested()) {	
		try {
				SimpleAudioPlayer();
				
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.clip.stop();
	}
	private void SimpleAudioPlayer() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		this.audioInputS = AudioSystem.getAudioInputStream(new File(getClass().getResource(filePath).getFile()));
		clip = AudioSystem.getClip();
		clip.open(audioInputS);
		if (!this.stopRequested) {
			clip.start();
			sleep(150000);	
		}
		else {
			clip.stop();
		}
	}
}
