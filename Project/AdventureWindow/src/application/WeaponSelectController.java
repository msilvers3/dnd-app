package application;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.Button;
/**
 * this is the controller class for weapons and handles how the weapon affects the events within the game
 * @author TeamOmega
 *
 */
public class WeaponSelectController {
	// Create your objects but not initialize them
	Player player1;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	ObservableList<String> obsWeapons;
	Alert alert = new Alert(AlertType.INFORMATION);
	// Create your controllers but not initialize them
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	WeaponSelectController wpnCon;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button selectWpnButton;

    @FXML
    private TextArea weaponInfoTxtArea;

    @FXML
    private ComboBox<String> listFoundWeaponsCombo;

    
    @FXML
    void handleSelectButton(ActionEvent event) throws IOException {
    	if (listFoundWeaponsCombo.getSelectionModel().isEmpty()) {
    		System.out.println("Nothing selected");
    	}
    	else {
    		for (Weapon weapon:this.weapons.getWeaponsList()) {
    			if (listFoundWeaponsCombo.getSelectionModel().getSelectedItem() == weapon.getItemName()) {
    				player1.setWeapon(weapon);
    			}
    		}
    		backToPath(event);
    	}
    }
    
    @FXML
    void handleWeaponsCombo(ActionEvent event) {
    	for (Weapon weapon:this.weapons.getWeaponsList()) {
    		if (listFoundWeaponsCombo.getSelectionModel().getSelectedItem() == weapon.getItemName()) {
    			weaponInfoTxtArea.setText(weapon.toString());
    		}
    	}
    }
    @FXML
    void initialize() {
        assert listFoundWeaponsCombo != null : "fx:id=\"listFoundWeaponsCombo\" was not injected: check your FXML file 'WeaponSelectWindow.fxml'.";
        obsWeapons = FXCollections.observableArrayList();
        int count = 0;
        for (Weapon weapon:this.weapons.getWeaponsList()) {
        	obsWeapons.add(count, weapon.getItemName());
        	count++;
        }
        listFoundWeaponsCombo.setItems(obsWeapons);
    }

    
    
    private void backToPath(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("Path.fxml"));
		
    	// Set the path controller objects
    	pathController.setControllers(mmController, pathController, combatController, wpnCon);
    	pathController.setMainObjects(this.player1, locObject, events, entities, items, weapons);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(pathController);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 600,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.show();
    }
    
    // initialize the objects
    public void setMainObjects(Player player, Location location, EventModel events, EntityModel entities, ItemModel items, WeaponModel weapons) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    }
    
    // initialize the controllers
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.wpnCon = wpnCon;
    }
}