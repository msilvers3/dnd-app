package application;
import java.util.ArrayList;
/**
 * the event model class handles pulling information to make the event.
 * @author TeamOmega
 *
 */
public class EventModel {
	private ArrayList<Event> events;
	
	public EventModel() {
		this.events = new ArrayList<>();
	}
	
	
	public void addEvent(Event event) {
		this.events.add(event);
	}
	
	@Override
	public String toString() {
		String out = "";
		for(Event event: this.events) {
			out += event.toString() + "\n\n";
		}
		return out;
	}
	
	public Event getLastEvent() {
		Event lastEvent = this.events.get(this.events.size()-1);
		return lastEvent;
	}
}
