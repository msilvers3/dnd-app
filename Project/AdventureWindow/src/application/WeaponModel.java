package application;
import java.util.ArrayList;
/**
 * this class retrieves the weapon object and adds it to an array for other classes to retrieve later
 * @author TeamOmega
 *
 */
public class WeaponModel {
	private ArrayList<Weapon> weapons;
	
	public WeaponModel() {
		this.weapons = new ArrayList<>();
	}
	
	public void addWeapon(Weapon weapon) {
		this.weapons.add(weapon);
	}
	
	@Override
	public String toString() {
		String out = "";
		for (Weapon weapon:this.weapons) {
			out += weapon.toString() + "\n\n";
		}
		return String.format(
				"%s", out);
	}
	
	public ArrayList<Weapon> getWeaponsList() {
		return this.weapons;
	}
}
