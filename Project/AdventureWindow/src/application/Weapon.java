package application;
/**
 * this is the class that handles weapon object generation
 * @author TeamOmega
 *
 */
public class Weapon extends Item {
	private int bluntDamage;
	private int pierceDamage;
	private int magicDamage;
	private int totalDamage;
	/* Affinity 
	 * Numbers 1-3 for weapon affinity. 
	 * 1: Affinity for Strength builds 
	 * 2: Affinity for Intelligence builds
	 * 3: Affinity for Magic builds
	 * */
	private int affinity;
	
	
	public Weapon(String name, String description,int bluntDamage, int pierceDamage, int magicDamage, int affinity) {
		super(name,description);
		this.bluntDamage = bluntDamage;
		this.pierceDamage = pierceDamage;
		this.magicDamage = magicDamage;
		this.affinity = affinity;
	}

	public int getBluntDamage() {
		return bluntDamage;
	}
	public void setBluntDamage(int bluntDamage) {
		this.bluntDamage = bluntDamage;
	}
	public int getPierceDamage() {
		return pierceDamage;
	}
	public void setPierceDamage(int pierceDamage) {
		this.pierceDamage = pierceDamage;
	}
	public int getMagicDamage() {
		return magicDamage;
	}
	public void setMagicDamage(int magicDamage) {
		this.magicDamage = magicDamage;
	}
	
	public int getTotalDamage() {
		
		if (this.affinity ==1) {
			return (this.bluntDamage*2)+this.pierceDamage+this.magicDamage;
		}
		else if (this.affinity == 2 ) {
			return this.bluntDamage+(this.pierceDamage*2)+this.magicDamage;
		}
		else if (this.affinity == 3 ) {
			return this.bluntDamage+this.pierceDamage+(this.magicDamage*2);
		}
		else {
			return this.bluntDamage+this.pierceDamage+this.magicDamage;
		}
	}
	
	public String getAffinity() {
		if(this.affinity==1) {
			return "Strength";
		}
		else if (this.affinity==2) {
			return "Intelligence";
		}
		else if (this.affinity==3) {
			return "Magic";
		}
		else {
			return "None";
		}
	}
	@Override
	public String toString() {
		return String.format(
				"Weapon Info\n"
				+ "Blunt: %d\n"
				+ "Pierce: %d\n"
				+ "Magic: %d\n"
				+ "Affinity: %s",
				this.bluntDamage,
				this.pierceDamage,
				this.magicDamage,
				getAffinity());
	}
	
}
