package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
/**
 * This class will handle what the player chooses on screen within the application, telling the rest of the classes when to initialize
 * @author TeamOmega
 *
 */
public class SelectionController {
	
	
	String name;
	int specialtySelected;
	int weaponSelected;
	// Create your objects but not initialize them
	Player player1;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	Thread thread1;
	Thread thread2;
	Thread thread3;
	Sound8BitAdventure sound8BitAdv;
	SoundRetroForest soundRetroForest;
	SoundBossBattle soundBossBattle;
	
	Alert alert = new Alert(AlertType.INFORMATION);
	// Create your controllers but not initialize them
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	SelectionController ssCon;
	WeaponSelectController wpnCon;
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button goButton;

    @FXML
    private TextField nameInput;

    @FXML
    private ComboBox<?> specialtyCombo;

    @FXML
    private ComboBox<?> weaponCombo;

    @FXML
    void handleGoButton(ActionEvent event) throws IOException {
    	this.name = this.nameInput.getText();
    	if (hasPicked()) {
    		Weapon weapon = generateWeapon();
    		weapons.addWeapon(weapon);
    		this.player1 = generatePlayer(this.name,weapon);
    		thread1.interrupt();
    		goToPath(event);
    	}
    	else {
    		System.out.println("Something's not been selected");
    	}
    }



    @FXML
    void initialize() {
        assert goButton != null : "fx:id=\"goButton\" was not injected: check your FXML file 'Selection.fxml'.";
        assert nameInput != null : "fx:id=\"nameInput\" was not injected: check your FXML file 'Selection.fxml'.";
        assert specialtyCombo != null : "fx:id=\"specialtyCombo\" was not injected: check your FXML file 'Selection.fxml'.";
        assert weaponCombo != null : "fx:id=\"weaponCombo\" was not injected: check your FXML file 'Selection.fxml'.";

    }
    
    
    private Weapon generateWeapon() {
    	if (this.weaponCombo.getSelectionModel().getSelectedItem().toString().equals("Beginner's Staff")) {
    		return new Weapon("Beginner's Staff","A staff for beginners",1,1,7,3);
    	}
    	else if (this.weaponCombo.getSelectionModel().getSelectedItem().toString().equals("Beginner's Sword")) {
    		return new Weapon("Beginner's Sword","A sword for beginners",4,5,1,1);
    	}
    	else {
    		return new Weapon("GunMace","Effective with smart and strong fellows",7,5,0,2);
    	}
    }
    private Player generatePlayer(String name, Weapon weapon) {
    	if (this.specialtyCombo.getSelectionModel().getSelectedItem().toString().equals("Knight")) {
    		Player knight = new Player(name, weapon);
        	knight.setHealth(150);
        	knight.setStrength(13);
        	knight.setIntelligence(7);
        	knight.setMagicPower(5);
        	knight.setLuck(4);
        	knight.setMaxHealth(150);
        	return knight;
    	}
    	else if (this.specialtyCombo.getSelectionModel().getSelectedItem().toString().equals("Mage")) {
    		Player mage = new Player(name, weapon);
        	mage.setHealth(85);
        	mage.setStrength(5);
        	mage.setIntelligence(10);
        	mage.setMagicPower(10);
        	mage.setLuck(4);
        	mage.setMaxHealth(85);
        	return mage;
    	}
    	else {
    		Player bard = new Player(name, weapon);
    		bard.setHealth(100);
        	bard.setStrength(7);
        	bard.setIntelligence(8);
        	bard.setMagicPower(3);
        	bard.setLuck(11);
        	bard.setMaxHealth(100);
        	return bard;
    	}
    	
    	
    }
    
    
    private boolean hasPicked() {
    	boolean namePicked = this.nameInput.getText().isBlank();
    	System.out.println(namePicked);
    	boolean specialtyPicked = this.specialtyCombo.getSelectionModel().isEmpty();
    	System.out.println(specialtyPicked);
    	boolean weaponPicked = this.weaponCombo.getSelectionModel().isEmpty();
    	System.out.println(weaponPicked);
    	if (!namePicked && !specialtyPicked && !weaponPicked) {
    		return true;
    	}
    	else {
    		return false;
    	}

    }
    
    private void goToPath(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("Path.fxml"));
    	// Set the path controller objects
    	pathController.setControllers(mmController, pathController, combatController, wpnCon);
    	pathController.setMainObjects(player1, 
    			locObject, 
    			events, 
    			entities, 
    			items, 
    			weapons, 
    			thread1,
    			thread2,
    			thread3,
    			sound8BitAdv, 
    			soundRetroForest,
    			soundBossBattle);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(pathController);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 600,500);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.setTitle("On Path");
    	window.show();
    }
 // initialize the objects
    public void setMainObjects(Player player, 
    		Location location, 
    		EventModel events, 
    		EntityModel entities, 
    		ItemModel items, 
    		WeaponModel weapons,
    		Thread thread1,
    		Thread thread2,
    		Thread thread3,
    		Sound8BitAdventure s8ba,
    		SoundRetroForest srf,
    		SoundBossBattle sbb) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.thread1 = thread1;
    	this.thread2 = thread2;
    	this.thread3 = thread3;
    	this.sound8BitAdv = s8ba;
    	this.soundRetroForest = srf;
    	this.soundBossBattle = sbb;
    }
    
    // initialize the controllers
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, SelectionController ssCon, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.ssCon = ssCon;
    	this.wpnCon = wpnCon;
    }

}
