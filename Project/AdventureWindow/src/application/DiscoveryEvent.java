package application;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Math;
import java.util.ArrayList;
/**
 *this class will handle the random discovery events that can happen when choosing a path. 
 * @author TeamOmega
 *
 */
public class DiscoveryEvent extends Event {
	private String totalEvent;
	private String actionSelected;
	private String itemNameSelected;
	private String adverbSelected;
	private String adjectiveSelected;
	private String colorSelected;
	private Item item;
	
	private String[] actions = {"discovers a(n)",
								"comes across a(n)",
								"stumbles into a(n)"};
	private String[] items = {	"health potion","bundle of clothes with a particular insignia","scroll of knowledge",
								"dead rat","heavy rock","worn boot","staff","club","sword","gunmace","scroll of strength",
								"scroll of magic"};
	
	private String[] adverbs = {"at the entrance of a(n)","at the bottom of a(n)","at the edge of a(n)",
								"near the tip of a(n)","near the bottom of a(n)","near the entrance to a(n)",
								"from the base of the","close by the","nearby the","just by the"};
	
	private String[] adjectives = {"tattered","worn","shiny",
									"sturdy","rigid","curved","bent"};
	
	private ArrayList<String> colorList; 	
	
	
	
	public DiscoveryEvent(Player player, Location location) {
		super(player, location);
		this.colorList = new ArrayList<>();
		generateListOfColors();
		generateRandomDiscoveryEvent();
		this.item = generateRandomItem();
	}
	
	public void generateRandomDiscoveryEvent() {
		int randomAction = (int)(Math.random()*this.actions.length);
		int randomItem = (int)(Math.random()*this.items.length);
		int randomAdverb = (int)(Math.random()*this.adverbs.length);
		int randomAdjective = (int)(Math.random()*this.adjectives.length);
		int randomColor = (int)(Math.random()*this.colorList.size());
		super.getLocation().generateRandomGeographicLocation();
		this.actionSelected = this.actions[randomAction];
		this.itemNameSelected = this.items[randomItem];
		this.adverbSelected = this.adverbs[randomAdverb];
		this.adjectiveSelected = this.adjectives[randomAdjective];
		this.colorSelected = this.colorList.get(randomColor);
		this.totalEvent = getPlayer().getName() + " " + this.actionSelected + " " + this.adjectiveSelected + " " + 
							this.itemNameSelected + " " + this.adverbSelected + " " +  getLocation().getGeoLocation();
		
		

	}
	
	/**
	 * this generates a random item when it is determined a discovery event will happen
	 * @return
	 */
	private Item generateRandomItem() {
		int randomStrengthPoint = (int)(Math.random()*super.getPlayer().getStrength());
		int randomIntelligencePoint = (int)(Math.random()*super.getPlayer().getIntelligence());
		int randomMagicPowerPoint = (int)(Math.random()*super.getPlayer().getMagicPower());
		int randomAffinity = (int)(Math.random()*4);
		String weaponName; 
		if (this.itemNameSelected.contains("staff")) {
			weaponName = String.format("%s %s Staff", this.colorSelected,this.adjectiveSelected);
			return new Weapon(weaponName, "Weapon",randomStrengthPoint, randomIntelligencePoint, randomMagicPowerPoint, randomAffinity);
		}
		else if (this.itemNameSelected.contains("sword")) {
			weaponName = String.format("%s %s Sword", this.colorSelected,this.adjectiveSelected);
			return new Weapon(weaponName, "Weapon",randomStrengthPoint, randomIntelligencePoint, randomMagicPowerPoint, randomAffinity);
		}
		else if (this.itemNameSelected.contains("club")) {
			weaponName = String.format("%s %s Club", this.colorSelected,this.adjectiveSelected);
			return new Weapon(weaponName, "Weapon",randomStrengthPoint, randomIntelligencePoint, randomMagicPowerPoint, randomAffinity);
		}
		else if (this.itemNameSelected.contains("gunmace")) {
			weaponName = String.format("%s %s Gunmace", this.colorSelected,this.adjectiveSelected);
			return new Weapon(weaponName, "Weapon",randomStrengthPoint, randomIntelligencePoint, randomMagicPowerPoint, randomAffinity);
		}
		else {
			return new Item(this.itemNameSelected, "Item");
		}
		
	}
	/**
	 * gives an item a color
	 */
	private void generateListOfColors() {
		try { 
			BufferedReader reader = new BufferedReader(new FileReader("src/application/masterColorList.txt"));
			String line;
			while((line = reader.readLine()) != null) {
				this.colorList.add(line);
			}
			reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public String getDiscoveryEventMessage() {
		return this.totalEvent;
	}
	
	public String getitemNameSelected() {
		return this.itemNameSelected;
	}
	
	@Override
	public String toString() {
		return String.format(
				"Discovery Event\n"
				+ "Info\n"
				+ "Action: %s\n",
				this.totalEvent);
	}
}
