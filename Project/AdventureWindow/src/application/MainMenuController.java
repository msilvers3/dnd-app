package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
/**
 * this is the controller that will handle the main menu, consisting of where it takes you when you choose the different options.
 * @author TeamOmega
 *
 */
public class MainMenuController {
	// Create your objects but not initialize them
	Player player1;
	Location locObject;
	EventModel events;
	EntityModel entities;
	ItemModel items;
	WeaponModel weapons;
	Thread thread1;
	Thread thread2;
	Thread thread3;
	Sound8BitAdventure sound8BitAdv;
	SoundRetroForest soundRetroForest;
	SoundBossBattle soundBossBattle;
	
	
	// Create your controllers
	MainMenuController mmController;
	PathController pathController;
	CombatController combatController;
	SelectionController ssCon;
	WeaponSelectController wpnCon;
	@FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    
    @FXML Label leftArrowLabel;
    
    @FXML
    private Hyperlink startHL2;

    
    
    @FXML
    private Hyperlink startHL;

    @FXML
    void handleOnMouseEnter(MouseEvent event) {
    	if (event.getEventType() == event.MOUSE_ENTERED) {
    		leftArrowLabel.setVisible(true);
    	}
    }
    @FXML
    void handleOnMouseEnter2(MouseEvent event) {

    }

    @FXML
    void handleOnMouseExit(MouseEvent event) {
    	if (event.getEventType() == event.MOUSE_EXITED) {
    		leftArrowLabel.setVisible(false);
    	}
    }
    @FXML
    void handleOnMouseExit2(MouseEvent event) {

    }
    
    @FXML
    void handleStartHL(ActionEvent event) throws IOException, InterruptedException {
//    	thread1.interrupt();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("Selection.fxml"));
    	// Set the path controller objects
    	ssCon.setControllers(mmController, pathController, combatController,ssCon, wpnCon);
    	ssCon.setMainObjects(player1, 
    			locObject, 
    			events, 
    			entities, 
    			items, 
    			weapons, 
    			thread1,
    			thread2,
    			thread3,
    			sound8BitAdv, 
    			soundRetroForest,
    			soundBossBattle);
    	if (loader.getController() != null) {
    		System.out.println("already has controller");
    	}
    	else {
    		loader.setController(ssCon);
    	}
    	Parent parent = loader.load();
    	Scene scene = new Scene(parent, 400,400);
    	Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
    	window.setScene(scene);
    	window.setTitle("Create Player");
    	window.show();
    	
    }
    @FXML
    void handleStartHL2(ActionEvent event) {

    }


    @FXML
    void initialize() {
        assert startHL != null : "fx:id=\"startHL\" was not injected: check your FXML file 'MainMenuWindow.fxml'.";
        Sound8BitAdventure sound8BitAdv = new Sound8BitAdventure();
		SoundRetroForest soundRetroForest = new SoundRetroForest();
		SoundBossBattle soundBossBattle = new SoundBossBattle();
		
		thread1 = new Thread(sound8BitAdv, "Adventure Start Running");
		thread2 = new Thread(soundRetroForest, "Path Sound Running");
		thread3 = new Thread(soundBossBattle, "Boss Sound Running");
		thread1.start();
		
		Weapon fist = new Weapon("Fist","",1,0,0,1);
		fist.setDescription("Typically used for hand-to-hand combat.\nWould be ineffective in any other situation.");
		Player player1 = new Player("Player",fist);
		EntityModel entities = new EntityModel();
		EventModel events = new EventModel();
		ItemModel items = new ItemModel();
		Location locObject = new Location();
		WeaponModel weapons = new WeaponModel();
		weapons.addWeapon(fist);
		
    }
    public void setThreads(Thread thread1,
    		Thread thread2,
    		Thread thread3,
    		Sound8BitAdventure s8ba,
    		SoundRetroForest srf,
    		SoundBossBattle sbb)	{
    	this.thread1 = thread1;
    	this.thread2 = thread2;
    	this.thread3 = thread3;
    	this.sound8BitAdv = s8ba;
    	this.soundRetroForest = srf;
    	this.soundBossBattle = sbb;
    
    }
    public void setMainObjects(Player player, 
    		Location location, 
    		EventModel events, 
    		EntityModel entities, 
    		ItemModel items, 
    		WeaponModel weapons,
    		Thread thread1,
    		Thread thread2,
    		Thread thread3,
    		Sound8BitAdventure s8ba,
    		SoundRetroForest srf,
    		SoundBossBattle sbb) {
    	this.player1 = player;
    	this.locObject = location;
    	this.events = events;
    	this.entities = entities;
    	this.items = items;
    	this.weapons = weapons;
    	this.thread1 = thread1;
    	this.thread2 = thread2;
    	this.thread3 = thread3;
    	this.sound8BitAdv = s8ba;
    	this.soundRetroForest = srf;
    	this.soundBossBattle = sbb;
    }
    
    public void setControllers(MainMenuController mmController, PathController pathController, CombatController combatController, SelectionController ssCon, WeaponSelectController wpnCon) {
    	this.mmController = mmController;
    	this.pathController = pathController;
    	this.combatController = combatController;
    	this.ssCon = ssCon;
    	this.wpnCon = wpnCon;
    }
    


}